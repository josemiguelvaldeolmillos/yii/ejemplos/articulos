# eliminar la base de datos
DROP DATABASE IF EXISTS articulos;

/*
crear y seleccionar la base de datos

*/
CREATE DATABASE articulos; -- creamos base de datos

USE articulos; -- decimos que use la base articulos

CREATE TABLE articulos(
	id INT AUTO_INCREMENT,
	nombre VARCHAR(200),
	categoria INT,
	precio FLOAT,
	stock BOOL,
	fecha DATE,
	PRIMARY KEY(id)
);



CREATE TABLE categorias(
 	id INT,
 	nombre VARCHAR(200),
 	descripcion VARCHAR(500),
 	numero INT,
 	PRIMARY KEY(id)
);
categoriascategoriascategoriasarticulosclientesclientesarticulosarticulos