<?php

/** @var yii\web\View $this */

$this->title = 'Gestion de productos';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Gestión de Articulos</h1>

        <p class="lead">Aplicación para realizar la gestión de mis productos y de las categorias.</p>

        
    </div>

    <?= \yii\helpers\Html::img("@web/imgs/articulos.jpg") ?>
    
</div>
